/**************************************
  Names: Landon Byrd and Alexa Parker
  Course: CPSC 3300
  Project: 2
  Due: 15 March 2018
***************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define SIGNALNUM 19
enum signal_enum {
                  ip_, lp_, ep_, lm_, r_,
                  w_, ld_, ed_, li_, ei_,
                  la_, ea_, a_, s_, eu_, lb_,
                  cd_, map_, hlt_, crja_
                };


unsigned short enable_type;

enum enable_num {
    enable_ep,
    enable_ed,
    enable_ei,
    enable_ea,
    enable_eu
};


int order_of_op[]= {ep_,ed_,ei_,ea_,eu_,r_, w_,
                  a_, s_, lp_, lm_, ld_, li_,
                  la_, lb_, ip_, cd_, crja_,
                   map_, hlt_};
const char signals[][SIGNALNUM]  ={"ip", "lp", "ep", "lm", "r", "w",
                            "ld", "ed", "li", "ei", "la", "ea",
                             "a", "s", "eu", "lb", "cd", "map",
                            "hlt"};

int q = 0;

/* registers and memory - represented by 32-bit int data type even though */
/*   most registers and memory have a 12-bit word size; note that the pc  */
/*   and mar hold 8-bit addresses only */

int halt     = 0, /* halt flag to halt the simulation */
    pc       = 0, /* program counter register, abbreviated as p */
    mar      = 0, /* memory address register, abbreviated as m */
    ram[256],     /* main memory to hold instructions and data */
    mdr      = 0, /* memory data register, abbreviated as d */
    acc      = 0, /* accumulator register, abbreviated as a */
    alu_tmp  = 0, /* called "ALU" register in the paper, abbreviated as u */
    b        = 0, /* b register to hold second operand for add/subtract */
    ir       = 0, /* instruction register, abbreviated as i */
    ja       =0;

    int rom_addr[256]; /*stores rom addresses*/
    int rom[256];      /*stores rom data values*/

int word_count; /* indicates how many memory words to display at end */
int addr_count;
int inst_count;

int uc = 0;
int uir = 0;
long long int cur_int =0;

//uir = control_rom[uc]
//decode -obtain control signals
//execute - apply control signals - part of this is determining value of uc. Every time gone through update uc Either crja or address rom process in correct order
//at microinstruction level
//look at diagram

/* initialization routine to read in memory contents */

void load_ram(){
  int i = 0;
  FILE *fp;

  if( ( fp = fopen( "ram.txt", "r" ) ) == NULL ){
    printf( "error in opening ram file\n" );
    exit( -1 );
  }
  printf( "contents of RAM memory\n" );
  printf( "addr value\n" );
  while( fscanf( fp, "%x", &ram[i] ) != EOF ){
    if( i >= 256 ){
      printf( "ram.txt program file overflows available RAM\n" );
      exit( -1 );
    }
    ram[i] = ram[i] & 0xfff; /* clamp to 12-bit word size */
    printf( " %2x:  %03x\n", i, ram[i] );
    i++;
  }
  word_count = i;
  for( ; i < 256; i++ ){
    ram[i] = 0;
  }
  printf( "\n" );
  fclose( fp );
}


void load_rom_addr(){
  int i = 0;
  FILE *fp;

  if( ( fp = fopen( "addr.txt", "r" ) ) == NULL ){
    printf( "error in opening rom addr file\n" );
    exit( -1 );
  }
  printf( "contents of address ROM\n" );
  printf( "opc addr\n" );
  while( fscanf( fp, "%x", &rom_addr[i] ) != EOF ){
    if( i >= 256 ){
      printf( "addr.txt program file overflows available address space for ROM\n" );
      exit( -1 );
    }
    rom_addr[i] = rom_addr[i] & 0xfff; /* clamp to 12-bit word size */
    printf( " %2x:  %02x\n", i, rom_addr[i] );
    i++;
  }
  addr_count = i;
  for( ; i < 256; i++ ){
    rom_addr[i] = 0;
  }
  printf( "\n" );
  fclose( fp );
}

void load_rom_data(){
  long long int bit_mask = 0x800000;
  int i = 0;
  FILE *fp;

  if( ( fp = fopen( "uprog.txt", "r" ) ) == NULL ){
    printf( "error in opening rom addr file\n" );
    exit( -1 );
  }
  printf( "contents of control ROM with active signals identified\n" );
  printf( "addr contents\n" );
  while( fscanf( fp, "%x", &rom[i] ) != EOF ){
    if( i >= 256 ){
      printf( "addr.txt program file overflows available address space for ROM\n" );
      exit( -1 );
    }


    printf( " %2x:  %06x", i, rom[i] );
    for(int j=0;j<SIGNALNUM;j++){
      if(rom[i] & (bit_mask>>j)){
        printf("%3s",signals[j]);
      }
      else{
        printf("%3s","");
      }
    }
    printf(" crja=%02x\n",rom[i] & 0x1f);
    i++;
  }
  inst_count = i;
  for( ; i < 256; i++ ){
    rom_addr[i] = 0;
  }
  printf( "\n" );
  fclose( fp );
}

long long int get_load_type(){
  long long int load_val;

  switch(enable_type){

    case enable_ep:
    load_val = pc;
    break;

    case enable_ed:
    load_val = mdr;
    break;

    case enable_ea:
    load_val = acc;
    break;

    case enable_ei:
    load_val = ir & 0XFF;
    break;

    case enable_eu:
    load_val = alu_tmp;
    break;
  }

  return load_val;
}



//increment pc
void IP(){
  pc++;
}

//load pc
void LP(){
  long long int load_val = get_load_type();
  pc= load_val & 0xFF;
}

//enable pc
void EP(){
  enable_type = enable_ep;
}

//load mar
void LM(){
  long long int load_val = get_load_type();
  mar = load_val & 0xFF;
}

//read
void R(){
  mdr = ram[ mar ];
}

//write
void W(){
  ram[mar] = mdr;
}

//load mdr
void LD(){
  long long int load_val = get_load_type();
  mdr = load_val & 0xFFF;
}

//enable mdr
void ED(){
  enable_type = enable_ed;
}

//load ir
void LI(){
  long long int load_val = get_load_type();
  ir = load_val & 0xFFF;
}

//enable ir
void EI(){
  enable_type = enable_ei;
}

void LA(){
  long long int load_val = get_load_type();
  acc = load_val & 0xFFF;
}

//enable accumulator
void EA(){
  enable_type = enable_ea;
}

//add
void A(){
  alu_tmp = ( acc + b ) & 0xfff; /* clamp to 12-bit word size */
  acc = alu_tmp;
}

//subtract
void S(){
  alu_tmp = ( acc - b ) & 0xfff; /* clamp to 12-bit word size */
  acc = alu_tmp;
}

//enable alu temp register
void EU(){
  enable_type = enable_eu;
}

//load b register
void LB(){
  long long int load_val = get_load_type();
  b = load_val & 0xFFF;
}

//conditional jump
void CD(){
  if( acc >> 11 ){
    uc= cur_int & 0x1f;
  }
}

//use address ROM to map opcode in IR to starting address of microroutine
void MAP(){
  uc = rom_addr[(ir & 0xF00) >>8];
}

void HALT(){
  halt =1;
}

void ( * sigfuncs[19] )() = { IP, LP, EP, LM, R, W,
                            LD, ED, LI, EI, LA, EA,
                             A, S, EU, LB, CD, MAP, HALT};

void sigdecode(int signum){
  (*sigfuncs[signum])();
}

int main(){
  void ( *inst )();
  int i;

  printf( "microprogram simulation of Eckert's simple machine\n" );
  printf( "(all values are shown in hexadecimal)\n\n" );

  load_ram();
  load_rom_addr();
  load_rom_data();

  printf( "intial register values\n" );
  printf( " pc mar mdr acc alu   b  ir\n" );
  printf( "%3x %3x %3x %3x %3x %3x %3x\n\n",
    pc, mar, mdr, acc, alu_tmp, b, ir );

  printf( "control signals and register values after each microinstruction\n" );
  printf( "                     mh\n");
  printf( "   ilelrwleleleasel cal\n");
  printf( "uc pppm  ddiiaa  ub dpt ja pc mar mdr acc alu   b  ir\n\n" );

  while( !halt ){
    //execute signals
    long long int bit_mask = 0x800000;

    cur_int = rom[uc];

    for(int j=0;j<SIGNALNUM+1;j++){
      int current_op = order_of_op[j];

      if(current_op == crja_){
        ja = cur_int & 0x1f;
      }else if(cur_int & (bit_mask>>current_op)){
        sigdecode(order_of_op[j]);
      }
    }

    ja = cur_int & 0x1F;
    if(!((bit_mask >> map_) & cur_int) && !((bit_mask >> cd_) & cur_int)) uc = ja;


    //print out binary values
    printf("%02x ",uc);

    for(int j=0;j<16;j++){
      if(cur_int & (bit_mask>>j)){
        printf("1");
      } else {
        printf("0");
      }
    }

    printf(" ");

    for(int j=16;j<19;j++){
      if(cur_int & (bit_mask>>j)){
        printf("1");
      } else {
        printf("0");
      }
    }

    printf( "%02x %3x %3x %3x %3x %3x %3x %3x",
      ja, pc, mar, mdr, acc, alu_tmp, b, ir );

    for(int j=0;j<SIGNALNUM;j++){
      if(cur_int & (bit_mask>>j)){
        printf(" %s",signals[j]);
      }
    }

    printf("\n");
    if(uc ==0){
      printf("\n");
    }
    // q++;
    // if(q==30) halt = 1;
  }

  printf( "contents of RAM memory\n" );
  printf( "addr value\n" );
  for( i = 0; i < word_count; i++ ){
    printf( " %2x:  %03x\n", i, ram[i] );
  }
  printf( "\n" );

  return 0;
}
